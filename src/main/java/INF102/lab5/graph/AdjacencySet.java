package INF102.lab5.graph;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class AdjacencySet<V> implements IGraph<V> {

    private Set<V> nodes;
    private Map<V, Set<V>> nodeToNode;

    public AdjacencySet() {
        nodes = new HashSet<>();
        nodeToNode = new HashMap<>();
    }

    @Override
    public int size() {
        return nodes.size();
    }

    @Override
    public Set<V> getNodes() {
        return Collections.unmodifiableSet(nodes);
    }

    @Override
    public void addNode(V node) {
        throw new UnsupportedOperationException("Implement me :)");
    }

    @Override
    public void removeNode(V node) {
        throw new UnsupportedOperationException("Implement me :)");
    }

    @Override
    public void addEdge(V u, V v) {
        throw new UnsupportedOperationException("Implement me :)");
    }

    @Override
    public void removeEdge(V u, V v) {
        throw new UnsupportedOperationException("Implement me :)");
    }

    @Override
    public boolean hasNode(V node) {
        throw new UnsupportedOperationException("Implement me :)");
    }

    @Override
    public boolean hasEdge(V u, V v) {
        return nodeToNode.get(u).contains(v);
    }

    @Override
    public Set<V> getNeighbourhood(V node) {
        return Collections.unmodifiableSet(nodeToNode.get(node));
    }

    @Override
    public String toString() {
        StringBuilder build = new StringBuilder();
        for (V node : nodeToNode.keySet()) {
            Set<V> nodeList = nodeToNode.get(node);

            build.append(node);
            build.append(" --> ");
            build.append(nodeList);
            build.append("\n");
        }
        return build.toString();
    }

}
